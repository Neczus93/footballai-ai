'''
Created on Apr 23, 2020

@author: macbookpro
'''

from sqlalchemy import create_engine
import pandas as pd
# import xgboost as xgb #TODO

from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC
from IPython.display import display

# Load data from DATABASE

db_connection_str = 'mysql+pymysql://neczus:S0L9ibe3@footballai-db.czijux9491gw.eu-central-1.rds.amazonaws.com:3306/innodb'
db_connection = create_engine(db_connection_str)

data = pd.read_sql('SELECT * FROM `MATCH`', con=db_connection)

display(data.head())



# Total number of matches.
n_matches = data.shape[0]

# Calculate number of features. -1 because we are saving one as the target variable (win/lose/draw)
n_features = data.shape[1] - 1

# Calculate matches won by home team.
# n_homewins = len(data[data.FTR == 'H'])

# Calculate win rate for home team.
# win_rate = (float(n_homewins) / (n_matches)) * 100

# Print the results
print ("Total number of matches: {}".format(n_matches))
print ("Number of features: {}".format(n_features))
# print "Number of matches won by home team: {}".format(n_homewins)
# print "Win rate of home team: {:.2f}%".format(win_rate)



